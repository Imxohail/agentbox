<?php

// load contents from XML file
$xml = simplexml_load_file("sample-reaxml.xml");

// parse as JSON
$json = json_encode($xml);

// parse as Array
$array = json_decode($json,TRUE);

$result = array();
foreach ($array as $key => $value) {
	// iterate over nodes only
	if($key !== '@attributes'){
		$sub_array = $value;
		
		// If only one index
		if(isset($sub_array['uniqueID'])){
			$result[$sub_array['uniqueID']] = $key;
		}
		else{
			// iterate over nodes sub-arrays
			foreach ($sub_array as $value) {
				$result[$value['uniqueID']] = $key;
			}
		}
	}
}

// echo '<pre>'.print_r($result, true).'</pre>';
