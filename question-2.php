<?php

$str1 = 'MICHAEL';
$str2 = 'JACKSON';
$result = '';

// get the length of longest string
$longest = max(strlen($str1), strlen($str2));

// split each string into array
$array1 = str_split($str1);
$array2 = str_split($str2);

// if value exists for the index concatenate it to 
// the string one by one
for($i = 0; $i < $longest; $i++){
    if(array_key_exists($i, $array1)){
        $result .= $array1[$i];
    }

    if(array_key_exists($i, $array2)){
        $result .= $array2[$i];
    }
}

print $result;