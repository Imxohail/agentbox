<?php

$str1 = 'Michael is here';
$str2 = 'Michael';

// strpos() returns either the offset at which the needle string begins
// in the haystack string, or the boolean false if the needle isn't found.
if (strpos($str1,$str2) !== false) {
    echo "\"" . $str1 . "\" contains \"" . $str2 . "\"";
}
else{
	echo "\"" . $str1 . "\" does not contain \"" . $str2 . "\"";	
}
