<?php

// load contents from XML file
$xml = simplexml_load_file("sample-reaxml.xml");

// parse as JSON
$json = json_encode($xml);

// parse as Array
$array = json_decode($json,TRUE);

$result = array();
foreach ($array as $key => $value) {
	// iterate over nodes only
	if($key !== '@attributes'){
		$sub_array = $value;
		// If only one index
		if(isset($sub_array['address'])){
			$state = $sub_array['address']['state'];
			if(isset($sub_array['price'])){
				$result[$key][$state][] = $sub_array['price'];
			}
		}
		else{
			// iterate over nodes sub-arrays
			foreach ($sub_array as $value) {
				$state = $value['address']['state'];
				if(isset($value['price'])){
					// if there is a range
					if(isset($value['price']['range'])){
						$average = ($value['price']['range']['min']+$value['price']['range']['max'])/2;
						$result[$key][$state][] = $average;
					}
					else{
						$result[$key][$state][] = $value['price'];	
					}
				}
			}
		}
	}
}

// echo '<pre>'.print_r($result, true).'</pre>';
